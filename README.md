# FastAPI Application

This project is a FastAPI application that provides endpoints to receive random car data in a WebSocket connection and fetch images encoded in base64 format based on the provided number.

## Features

- WebSocket connection to receive continuous updates of car data.
- POST endpoint to receive a specified number of images encoded in base64.
- Docker Compose for easy setup and deployment.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Docker
- Docker Compose

### RUN
Go to folder with docker-compose.yaml

```bash
docker-compose up --build
```
This command will start the FastAPI application and any other services defined in your docker-compose.yml file.


# Endpoint Testing

## WebSocket Connection for Car Data
Establish a WebSocket connection to the endpoint or run receive_data.py:
```bash
ws://localhost:8000/ws
```
You will start receiving random car data at the interval specified by the TIME_PERIOD environment variable.

## Fetching Images
To fetch images, make a POST request to the /get-image endpoint with a JSON payload specifying the number of images:
```bash
curl -X 'POST' \
  'http://localhost:8000/get-image' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "number": 5
}'
```
This request will return a JSON response containing the specified number of images encoded in base64.

## Stopping the WebSocket Data Stream
To stop receiving data from the WebSocket connection, make a GET request to the /stop endpoint:
```bash
curl -X 'GET' 'http://localhost:8000/stop'
```