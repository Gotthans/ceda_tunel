from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from typing import List
from pydantic import BaseModel
from datetime import datetime
import random, string, asyncio, os
from dotenv import load_dotenv
from app.modules.image import IMAGE_BASE64

load_dotenv()

app = FastAPI()
background_task_running = False

class GPSPosition(BaseModel):
    latitude: float
    longitude: float

class Car(BaseModel):
    id: str
    timestamp: str  # ISO 8601 format
    gps_position: GPSPosition
    license_plate: str

class CarsResponse(BaseModel):
    cars: List[Car]

class ImageRequest(BaseModel):
    number: int

class ImageData(BaseModel):
    image_base64: str
    timestamp: str

class ImagesResponse(BaseModel):
    images: List[ImageData]



def generate_random_car_data() -> Car:
    return Car(
        id=''.join(random.choices(string.ascii_uppercase + string.digits, k=6)),
        timestamp=datetime.now().isoformat(),
        gps_position=GPSPosition(latitude=round(random.uniform(-90, 90), 6), longitude=round(random.uniform(-180, 180), 6)),
        license_plate=''.join(random.choices(string.ascii_uppercase + string.digits, k=7))
    )

async def send_random_data(websocket: WebSocket, time_period: float, max_cars: int = 50):
    global background_task_running
    background_task_running = True
    
    while background_task_running:
        # Generate a list of random car data up to max_cars
        cars_data = [generate_random_car_data() for _ in range(random.randint(1, max_cars))]
        
        try:
            await websocket.send_json({"cars": [car.dict() for car in cars_data]})
        except WebSocketDisconnect:
            background_task_running = False
        
        await asyncio.sleep(time_period)


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    time_period = float(os.getenv("TIME_PERIOD", "0.5"))
    await send_random_data(websocket, time_period, max_cars=50)


@app.get("/stop")
async def stop_sending():
    global background_task_running
    background_task_running = False
    return {"message": "Stopped sending data."}


@app.post("/get-image", response_model=ImagesResponse)
async def get_image(request: ImageRequest):
    images_list = []
    for i in range(request.number):  # Assuming request.number indicates the number of images
        # Simulate loading and encoding different images
        encoded_image = IMAGE_BASE64 
        timestamp = datetime.now().isoformat()
        images_list.append(ImageData(image_base64=encoded_image, timestamp=timestamp))
    
    return ImagesResponse(images=images_list)

